﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Test.Migrations
{
    public partial class mg2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bills_Products_ProductId",
                table: "Bills");

            migrationBuilder.DropIndex(
                name: "IX_Bills_ProductId",
                table: "Bills");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "Bills");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "Bills",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Bills_ProductId",
                table: "Bills",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bills_Products_ProductId",
                table: "Bills",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
