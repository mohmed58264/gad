﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Test.Migrations
{
    public partial class mg3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BasketId",
                table: "Bills",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BasketId1",
                table: "Bills",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Bills_BasketId1",
                table: "Bills",
                column: "BasketId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Bills_Baskets_BasketId1",
                table: "Bills",
                column: "BasketId1",
                principalTable: "Baskets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bills_Baskets_BasketId1",
                table: "Bills");

            migrationBuilder.DropIndex(
                name: "IX_Bills_BasketId1",
                table: "Bills");

            migrationBuilder.DropColumn(
                name: "BasketId",
                table: "Bills");

            migrationBuilder.DropColumn(
                name: "BasketId1",
                table: "Bills");
        }
    }
}
