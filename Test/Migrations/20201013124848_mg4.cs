﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Test.Migrations
{
    public partial class mg4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "Baskets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Baskets_ApplicationUserId",
                table: "Baskets",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Baskets_AspNetUsers_ApplicationUserId",
                table: "Baskets",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Baskets_AspNetUsers_ApplicationUserId",
                table: "Baskets");

            migrationBuilder.DropIndex(
                name: "IX_Baskets_ApplicationUserId",
                table: "Baskets");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "Baskets");
        }
    }
}
