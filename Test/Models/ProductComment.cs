﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models
{
    public class ProductComment
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        [ForeignKey("ApplicationUserId")]
        public string ApplicationUserId { get; set; }
        
        public Product Product { get; set; }
        [ForeignKey("ProductId")]
        public int ProductId { get; set; }

    }
}