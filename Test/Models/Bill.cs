﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models
{
    public class Bill
    {
        public int Id { get; set; }
        public DateTime BillDate { get; set; }
        public string BillDay { get; set; }
        public float Discount { get; set; }
        public float Quantity { get; set; }
      
        public ApplicationUser ApplicationUser { get; set; }
        [ForeignKey("ApplicationUserId")]
        public string ApplicationUserId { get; set; }
        
        
          
        public Basket Basket { get; set; }
        [ForeignKey("BasketId")]
        public string BasketId { get; set; }
        
        
                        
        
    }
}