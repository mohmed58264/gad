﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "الرجاء ادخال اسم صحيح.", MinimumLength = 6)]
        [Display(Name = "اسم المستخدم")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "يجب ان تحتوي كلمة المرور على احرف وارقام", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تاكيد كلمة المرور ")]
        [Compare("Password", ErrorMessage = "كلمة المرور غير متوافقة.")]
        public string ConfirmPassword { get; set; }
        [Display(Name = "رقم الهاتف")]
        public  string PhoneNumber  { get; set; }
    }
}
