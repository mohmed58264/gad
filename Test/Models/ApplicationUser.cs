﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Test.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public  string PhoneNumber  { get; set; }
        public List<Booking> Bookings { get; set; }
        public List<Bill> Bills { get; set; }
        public List<ProductComment> ProductComments { get; set; }
        public List<Basket> Baskets { get; set; }
    }
}
