﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public float Price { get; set; }
        public string Details { get; set; }
        public float Discount { get; set; }
        public float Quantity { get; set; }
        public string ProductImage { get; set; }
        public Category Category { get; set; }
        [ForeignKey("CategoryId")]
        public int CategoryId { get; set; }
      public List<Basket> Baskets { get; set; }
      public List<ProductComment> ProductComments { get; set; }  
    }
}