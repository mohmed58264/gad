﻿namespace Test.Models
{
    public class ProductHistory
    {
        public int Id   { get; set; }
        public string ProductName {get; set; }
         public float Price { get; set;}
         public string Details { get; set;}
         public float Quantity { get; set; }
    }
}