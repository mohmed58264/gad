﻿using System.Collections.Generic;

namespace Test.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryImage { get; set; }
        public string CategoryDetails { get; set; }
        public List<Product> Products { get; set; }
    }
}