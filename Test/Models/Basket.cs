﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models
{
    public class Basket
    {
        public int Id { get; set; }
        public float Quantity { get; set; }
        public Product Product { get; set; }
        [ForeignKey("ProductId")]
        public int ProductId { get; set; }
        public List<Booking> Bookings { get; set; }
        public List<Bill>Bills { get; set; }
        
        public ApplicationUser ApplicationUser { get; set; }
        [ForeignKey("ApplicationUserId")]
        public string ApplicationUserId { get; set; }
    }
    
}