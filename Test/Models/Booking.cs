﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models
{
    public class Booking
    {
        public int Id { get; set; }
        public DateTime BookingDateTime { get; set; }
        public Basket Basket { get; set; }
        public int BasketId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        [ForeignKey("ApplicationUserId")]
        public string ApplicationUserId { get; set; }
    }
}