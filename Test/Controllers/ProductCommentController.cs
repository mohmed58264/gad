﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Test.Data;
using Test.Models;

namespace Test.Controllers
{
    public class ProductCommentController : Controller
    { 
        
        
       
        private ApplicationDbContext _applicationDbContext;

        public ProductCommentController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }    
        // GET
        public IActionResult Index()
        {
            var productComments = _applicationDbContext.ProductComments.ToList();
            return View(productComments);
        }
        [HttpGet]
        public IActionResult Create() 
        { 


            return View();

            
        }

        [HttpPost]
        public IActionResult Create( ProductComment productComment)
        {
            
            _applicationDbContext.Add(productComment);
            _applicationDbContext.SaveChanges();
            return View();
        }
        [HttpGet]
        public IActionResult Edit(int id)
            
        {
            var productComment = _applicationDbContext.ProductComments.Find(id);
            return View(productComment);
        }

        [HttpPost]
        public IActionResult Edit(ProductComment productComment  )
        {
            _applicationDbContext.ProductComments.Update(productComment);
            _applicationDbContext.SaveChanges();
            
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Delete(int productCommentId)
        {
            var productComment = _applicationDbContext.ProductComments.Find(productCommentId);

            return View(productComment);
        }
        [HttpPost]

        public IActionResult Delete(ProductComment productComment )
        {
            _applicationDbContext.ProductComments.Remove(productComment);
            _applicationDbContext.SaveChanges();
            return RedirectToAction("Index");
        }     
        
        
    }
}