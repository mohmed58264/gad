﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Test.Controllers
{
    public class UploadImage
    {
     
        public static class Upload
        {
            public static async Task<string> UploadImageAsync(IFormFile image)
            {
                
                if (image != null && image.Length > 0)
                {
                    var fileName = Path.GetFileName(image.FileName);
                    fileName = Guid.NewGuid().ToString();
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Upload", fileName + ".png");
                    using (var fileSteam = new FileStream(filePath, FileMode.Create))
                    {
                        await image.CopyToAsync(fileSteam);
                    }

                    return fileName + ".png";
                    
                }
                else
                {
                    return "No Image";
                }
            }
        }

    }
}