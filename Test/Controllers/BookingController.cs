﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Test.Data;
using Test.Models;

namespace Test.Controllers
{
    public class BookingController : Controller
    {
    
        private ApplicationDbContext _applicationDbContext;

        public BookingController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }    
        // GET
        public IActionResult Index()
        {
            var bookings = _applicationDbContext.Bookings.ToList();
            return View(bookings);
        }
        [HttpGet]
        public IActionResult Create()
        { 


            return View();

            
        }

        [HttpPost]
        public IActionResult Create( Booking booking)
        {
            _applicationDbContext.Add(booking);
            _applicationDbContext.SaveChanges();
            return View();
        }
        [HttpGet]
        public IActionResult Edit(int id)
            
        {
            var booking = _applicationDbContext.Bookings.Find(id);
            return View(booking);
        }

        [HttpPost]
        public IActionResult Edit(Booking booking  )
        {
            _applicationDbContext.Bookings.Update(booking);
            _applicationDbContext.SaveChanges();
            
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Delete(int bookingId)
        {
            var booking = _applicationDbContext.Bookings.Find(bookingId);

            return View(booking);
        }
        [HttpPost]
        public IActionResult Delete(Booking booking)
        {
            _applicationDbContext.Bookings.Remove(booking);
            _applicationDbContext.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}