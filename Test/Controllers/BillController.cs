﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Test.Data;
using Test.Models;

namespace Test.Controllers
{
    public class BillController : Controller
    {
  
        
        private ApplicationDbContext _applicationDbContext;

        public BillController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }    
        // GET
        public IActionResult Index()
        {
            var bills = _applicationDbContext.Bills.ToList();
            return View(bills);
        }
        [HttpGet]
        public IActionResult Create() 
        { 
            ViewData["ProductId"]= new SelectList(_applicationDbContext.Products,"Id","ProductName");
            ViewData["UserId"]= new SelectList(_applicationDbContext.Users,"Id","Name");



            return View();

            
        }

        [HttpPost]
        public IActionResult Create( Bill bill)
        {
            _applicationDbContext.Add(bill);
            _applicationDbContext.SaveChanges();
            return View();
        }
        [HttpGet]
        public IActionResult Edit(int id)
            
        {
            var bill = _applicationDbContext.Bills.Find(id);
            return View(bill);
        }

        [HttpPost]
        public IActionResult Edit(Bill bill  )
        {
            _applicationDbContext.Bills.Update(bill);
            _applicationDbContext.SaveChanges();
            
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Delete(int BillId)
        {
            var bill = _applicationDbContext.Bills.Find(BillId);

            return View(bill);
        }
        [HttpPost]

        public IActionResult Delete(Bill bill )
        {
            _applicationDbContext.Bills.Remove(bill);
            _applicationDbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}