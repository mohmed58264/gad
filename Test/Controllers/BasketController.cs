﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Test.Data;
using Test.Models;

namespace Test.Controllers
{
    public class BasketController : Controller
    {
        // GET
    
        private ApplicationDbContext _applicationDbContext;

        public BasketController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }    
        // GET
        public IActionResult Index()
        {
            var baskets = _applicationDbContext.Baskets.ToList();
            return View(baskets);
        }
        [HttpGet]
        public IActionResult Create() 
        { 

            ViewData["ProductId"]= new SelectList(_applicationDbContext.Products,"Id","ProductName");
             

            return View();

            
        }

        [HttpPost]
        public IActionResult Create( List<Product> products ,List<float> quantity, string UserId)
        {
            
            foreach (var product in products)
            {
                int i = 0;
                  var UserBasket = new Basket()
                {
                    ProductId = product.Id,
                    Quantity = quantity[i],
                    ApplicationUserId = UserId

                };
                  i++;
                  _applicationDbContext.Add(UserBasket);
            }
            
            _applicationDbContext.SaveChanges();
            return Json("OK");
        }
        [HttpGet]
        public IActionResult Edit(int id)
            
        {
            var basket = _applicationDbContext.Baskets.Find(id);
            return View(basket);
        }

        [HttpPost]
        public IActionResult Edit(Basket basket  )
        {
            _applicationDbContext.Baskets.Update(basket);
            _applicationDbContext.SaveChanges();
            
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Delete(int BasketId)
        {
            var basket = _applicationDbContext.Baskets.Find(BasketId);

            return View(basket);
        }
        [HttpPost]

        public IActionResult Delete(Basket basket )
        {
            _applicationDbContext.Baskets.Remove(basket);
            _applicationDbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}