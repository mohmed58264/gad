﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Test.Data;
using Test.Models;

namespace Test.Controllers
{
    public class CategoryController : Controller
    {
        private ApplicationDbContext _applicationDbContext;

        public CategoryController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }    
        // GET
        public IActionResult Index()
        {
            var categorys = _applicationDbContext.Categorys.ToList();
            return View(categorys);
        }
        [HttpGet]
        public IActionResult Create()
        { 


            return View();

            
        }

        [HttpPost]
        public  async Task<IActionResult> Create(Category category ,IFormFile Image )
        {
            
            category.CategoryImage = await UploadImage.Upload.UploadImageAsync(Image);
            _applicationDbContext.Add(category);
            _applicationDbContext.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Edit(int id)
            
        {
            var category = _applicationDbContext.Categorys.Find(id);
            return View(category);
        }

        [HttpPost]
        public IActionResult Edit(Category category  )
        {
            _applicationDbContext.Categorys.Update(category);
            _applicationDbContext.SaveChanges();
            
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Delete(int categoryId)
        {
            var categor = _applicationDbContext.Categorys.Find(categoryId);

            return View(categor);
        }
[HttpPost]
        public IActionResult Delete(Category category)
        {
            _applicationDbContext.Categorys.Remove(category);
            _applicationDbContext.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}