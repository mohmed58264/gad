﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Test.Data;
using Test.Models;

namespace Test.Controllers
{
    public class ProductController : Controller
    {
        
        private ApplicationDbContext _applicationDbContext;

        public ProductController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }    
        // GET
        public IActionResult Index()
        {
            var products = _applicationDbContext.Products.ToList();
            return View(products);
        }
      
        public static class Upload
        {
            public static async Task<string> UploadImageAsync(IFormFile image)
            {
                
                if (image != null && image.Length > 0)
                {
                    var fileName = Path.GetFileName(image.FileName);
                    fileName = Guid.NewGuid().ToString();
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Upload", fileName + ".png");
                    using (var fileSteam = new FileStream(filePath, FileMode.Create))
                    {
                        await image.CopyToAsync(fileSteam);
                    }

                    return fileName + ".png";
                    
                }
                else
                {
                    return "No Image";
                }
            }
        }

        [HttpGet]
        public IActionResult Create()
        { 
           
            ViewData["CategoryId"]= new SelectList(_applicationDbContext.Categorys,"Id","CategoryName");

            return View();

            
        }
        [HttpPost]
        public  async Task<IActionResult> Create( Product product ,IFormFile Image)
        {
            product.ProductImage = await Upload.UploadImageAsync(Image);
            _applicationDbContext.Add(product);
            _applicationDbContext.SaveChanges();
            return View();
        }
        [HttpGet]
        public IActionResult Edit(int id)
            
        {
            
            ViewData["CategoryId"]= new SelectList(_applicationDbContext.Categorys,"Id","CategoryName");

            var product = _applicationDbContext.Products.Find(id);
            Response.Cookies.Append("oldProductImage", product.ProductImage.ToString(),
                new CookieOptions()
                {
                    Expires = DateTime.Now.AddMinutes(10)
                });

            return View(product);
        }

        [HttpPost]
        public async  Task<IActionResult> Edit(Product product ,IFormFile Image )
        { 
            if (Image.Length != null)
            { 
                System.IO.File.Delete("Upload/"+Request.Cookies["oldProductImage"]);
                var fileName = await UploadImage.Upload.UploadImageAsync(Image);
                product.ProductImage = fileName;
    
                _applicationDbContext.Products.Update(product);
                _applicationDbContext.SaveChanges();

            } 

            
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Delete(int ProductId)
        {
            var product = _applicationDbContext.Products.Find(ProductId);

            return View(product);
        }
[HttpPost]

        public IActionResult Delete(Product product )
        {
            _applicationDbContext.Products.Remove(product);
            _applicationDbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}